package clikafka;

import org.apache.kafka.clients.producer.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.Scanner;

public class KafkaTestTool {

    public static void run() {

        String kafkaConfig = "";
        String topicKey = "";
        String topicName = null;
        String message= "";

        runCommandLine("env | grep KAFKA");
        System.out.println(" ");
        runCommandLine("env | grep TOPIC");
        Producer<String, String> producer = null;

        while(true) {
            Scanner scanner1 = new Scanner(new InputStreamReader(System.in));
            System.out.println(" ");
            System.out.println("KAFKA config env var: ");
            kafkaConfig = scanner1.nextLine();

            Scanner scanner2 = new Scanner(new InputStreamReader(System.in));
            System.out.println(" ");
            System.out.println("TOPIC env var: ");
            topicKey = scanner2.nextLine();

            topicName = System.getenv(topicKey);

            if( topicName == null) {
                System.out.println("wrong value for TOPIC env var: " + topicKey);
                continue;
            } else {
                System.out.println("topicName: " + topicName);
            }

            producer = connect(kafkaConfig);

            if (producer != null) {
                break;
            } else {
                System.out.println("connection error to: " + kafkaConfig);
                continue;
            }
        }

        while(true) {
            Scanner scanner3 = new Scanner(new InputStreamReader(System.in));
            System.out.println(" ");
            System.out.println("q=quit, Message: ");
            message = scanner3.nextLine();
            long time = System.currentTimeMillis();
            try {
                // PUT a message to topic

                final ProducerRecord<String, String> record =
                      new ProducerRecord<>(topicName, message);

                RecordMetadata metadata = producer.send(record).get();

                long elapsedTime = System.currentTimeMillis() - time;
                System.out.printf("sent record(key=%s value=%s) " +
                            "meta(partition=%d, offset=%d) time=%d\n",
                      record.key(), record.value(), metadata.partition(),
                      metadata.offset(), elapsedTime);

                System.out.println("Message sent successfully to topic: " + topicName);

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (message.equals("q")) {
                producer.close();
                return;
            }
        }

    }


    private static Producer<String, String> connect(String kafkaConfig) {

        try {

            System.out.println("kafkaConfig: " + kafkaConfig + "  - " +  new FileSystemResource(System.getenv(kafkaConfig)).getPath());

            Properties props = new Properties();
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream input = new FileInputStream(new FileSystemResource(System.getenv(kafkaConfig)).getPath());

            props.load(input);

            props.put("acks", "1");
            Producer<String, String> producer = new KafkaProducer<String, String>(props);

            return producer;

        } catch (Exception e){
          e.printStackTrace();
          return null;
        }
    }

    private static  void runCommandLine(String command){

        String[] cmd = {"bash","-c", command};

        try{
            String line;
            Process p = Runtime.getRuntime().exec(cmd);
            BufferedReader input =
                  new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
            input.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}